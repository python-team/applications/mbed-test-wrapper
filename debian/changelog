mbed-test-wrapper (1.0.0-5) unstable; urgency=medium

  * d/control:
    - Update standards version to 4.6.0, no changes needed.
  * d/copyright:
    - Refresh years of Debian copyright
  * d/s/lintian-overrides:
    - Add override for dropping .gitconfig
  * d/watch:
    - Update pattern for GitHub archive URLs

 -- Nick Morrott <nickm@debian.org>  Sun, 06 Feb 2022 06:06:29 +0000

mbed-test-wrapper (1.0.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control:
    - Update Maintainer field with new Debian Python Team contact address.
    - Update Vcs-* fields with new Debian Python Team Salsa layout.

  [ Debian Janitor ]
  * d/s/lintian-overrides:
    - Update renamed lintian tag names in lintian overrides.
  * d/u/metadata:
    - Remove obsolete field Contact (already present in machine-readable
      debian/copyright).

  [ Nick Morrott ]
  * d/control:
    - Declare compliance with Debian Policy 4.5.1
  * d/copyright:
    - Refresh years of Debian copyright
  * d/mbed-test-wrapper.lintian-overrides:
    - Add overrides for repeated-path-segment and a false positive
  * d/patches:
    - Add patch unusual-interpreter
  * d/salsa-ci.yml:
    - Add Salsa CI pipeline
  * d/s/lintian-overrides:
    - Drop unused override

 -- Nick Morrott <nickm@debian.org>  Sun, 07 Feb 2021 11:44:33 +0000

mbed-test-wrapper (1.0.0-3) unstable; urgency=medium

  * d/manpages:
    - Improve formatting of custom manpage

 -- Nick Morrott <nickm@debian.org>  Fri, 31 Jul 2020 20:29:26 +0100

mbed-test-wrapper (1.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Nick Morrott ]
  * d/control:
    - Bump Standards-Version to 4.5.0
    - Bump debhelper-compat level to 13
    - Add Rules-Requires-Root field, set to no
    - Update Uploaders with my Debian email address
  * d/copyright:
    - Refresh years of Debian copyright
  * d/gbp.conf:
    - Use pristine-tar
  * d/mbed-test-wrapper.1.md.in:
    - Refresh author email address

 -- Nick Morrott <nickm@debian.org>  Fri, 19 Jun 2020 16:51:01 +0100

mbed-test-wrapper (1.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #913977)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 28 Dec 2018 07:18:05 +0000
